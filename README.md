# TTHbbAnalysis-Fits

Reference Config fits: https://gitlab.cern.ch/atlasHTop/ttHbb-fits/-/tree/master/ttHbb_paper_2021/v092

To run the code, after compiling (see Setup: https://gitlab.cern.ch/TRExStats/TRExFitter), use the command:


trex-fitter "action" configfile.yaml

eg: To produce prefit plots: trex-fitter -d file.yaml 




## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.cern.ch/nekumari/tthbbanalysis-fits.git
git branch -M master
git push -uf origin master
```

